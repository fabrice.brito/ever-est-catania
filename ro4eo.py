import requests
import os
from mimetypes import MimeTypes
import json


class EOResearchObject:
    
    access_token = None
    
    end_point_authentication = 'https://sso.everest.psnc.pl/oauth2/userinfo?schema=openid'
    end_point_ros = 'https://vre.ever-est.eu/esb/everest/ROs'
    
    title = None
    description = None
    vrc = None
    aoi = None
    
    resources = []
    
    results = []
    
    def __init__(self, access_token, title, description, vrc, aoi):
        
        self.title = title
        self.description = description
        self.vrc = vrc 
        self.aoi = aoi
        self.access_token = access_token
        
        if not self.isTokenValid():
            
            return None
    
    def isTokenValid(self):
        
        headers = {'Authorization': 'Bearer %s' % self.access_token }
        
        r = requests.get(self.end_point_authentication, headers=headers)
        
        if r.status_code == 200:
            
            return True
        
        else:
            
            return False
        
    def add_resource(self, resourceUri):
        
        self.resources.append(resourceUri)
        
    def add_result(self, result):
        
        self.results.append(result)
        
    def publish(self):
        
        ro_payload = {
   "ro_title":self.title,
   "ro_desc":self.description,
   "ro_vrc":self.vrc,
   "internal":[

   ],
   "mode":"PUBLIC",
   "semanticAnnotation":[
      {
         "property":{
            "term":"hasGeometry",
            "vocabulary":"http://www.opengis.net/ont/geosparql#"
         },
         "semanticAnnotation":[
            {
               "property":{
                  "term":"type",
                  "vocabulary":"http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               },
               "IRI":"http://www.opengis.net/ont/sf#Polygon"
            },
            {
               "property":{
                  "term":"type",
                  "vocabulary":"http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               },
               "IRI":"http://www.opengis.net/ont/geosparql#Geometry"
            },
            {
               "property":{
                  "term":"asWKT",
                  "vocabulary":"http://www.opengis.net/ont/geosparql#"
               },
               "literal":self.aoi,
               "datatype":"http://www.opengis.net/ont/geosparql#wktLiteral"
            }
         ]
      }
   ]
   }
        
        ro_external = []
        for resource in self.resources:
            ro_external.append({'resourceUri': resource})  
        
        ro_payload['external'] = ro_external
        
        headers = {'Authorization': 'Bearer %s' % self.access_token, 
           'Content-Type': 'application/json', 
          'Slug': 'cde'}
        
        request_ro = requests.post(self.end_point_ros,
                           data=json.dumps(ro_payload),
                           headers=headers)
        
        if request_ro.status_code != 201:
            
            raise requests.ConnectionError("Expected status code 201, but got {}".format(request_ro.status_code))
        
        ro_end_point = str(request_ro.json()['ro_name'])
        
        mime = MimeTypes()
        
        for result in self.results:
            
            mime_type = mime.guess_type(result)
            
            headers = {'Authorization': 'Bearer %s' % self.access_token, 
               'Content-Type': mime_type[0], 
               'Accept': 'application/rdf+xml',
               'Slug': os.path.basename(result)}
        
            data = open(result, 'rb').read()

            request_result = requests.post(ro_end_point,
                           data=data,
                           headers=headers)
            
            if request_result.status_code != 201:
                
                raise requests.ConnectionError("Expected status code 201, but got {}".format(request_result.status_code))
                
        end_point = 'http://sandbox.rohub.org/rodl/service/solr/reindex-ro/?ro=%s' % ro_end_point  
        
        headers = {'Authorization': 'Bearer ZXNiOmF5WjNTd2tuU1FiNg=='}
        
        request = requests.post(end_point,
                           headers=headers)
        
        if request.status_code != 200:
                
                raise requests.ConnectionError("Expected status code 200, but got {}".format(request.status_code))
               
            
        return ro_end_point
        